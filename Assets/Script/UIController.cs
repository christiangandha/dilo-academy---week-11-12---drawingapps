﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public AffineTransformer affineTransformer;
    public Painter painter;

    //input matriks transformasi
    public InputField Mat_A, Mat_B, Mat_C, Mat_D;

    [SerializeField]
    private InputField DegreeInput = null;

    [SerializeField]
    private InputField ShearInputX = null, ShearInputY = null;

    [SerializeField]
    private InputField PolygonInput = null;
    public InputField PolygonInputField
    {
        get { return PolygonInput; }
    }

    [SerializeField]
    private Button TransformButton = null;
   
    

    // Start is called before the first frame update
    void Start()
    {
        TransformButton.onClick.AddListener(TransformButton_OnClicked);

        DegreeInput.onValueChanged.AddListener(DegreeInput_OnEdit);
        ShearInputX.onValueChanged.AddListener(ShearInput_OnEdit);
        ShearInputY.onValueChanged.AddListener(ShearInput_OnEdit);
        PolygonInput.onValueChanged.AddListener(PolygonInput_OnEdit);
    }
    
    public void PolygonInput_OnEdit(string value = null)
    {
        if (!string.IsNullOrEmpty(value))
        {
            int temp = int.Parse(value);

            temp--; //karena mulai dari 0
            painter.polygonCount = temp;
        }
        
    }
    public void CheckPolygonInput()
    {
        int temp = 0;
        int.TryParse(PolygonInputField.text, out temp);
        if (temp < 5)
        {
            PolygonInput.text = "5";
        }
    }

    public void TransformButton_OnClicked()
    {
        CheckInputFieldMatrix();
        affineTransformer.ExecuteAffineTransformation(float.Parse(Mat_A.text), float.Parse(Mat_B.text), float.Parse(Mat_C.text), float.Parse(Mat_D.text));
    }

    private void CheckInputFieldMatrix()
    {
        if(string.IsNullOrEmpty(Mat_A.text))
        {
            Mat_A.text = "1";
        }
        if (string.IsNullOrEmpty(Mat_B.text))
        {
            Mat_B.text = "0";
        }
        if (string.IsNullOrEmpty(Mat_C.text))
        {
            Mat_C.text = "0";
        }
        if (string.IsNullOrEmpty(Mat_D.text))
        {
            Mat_D.text = "1";
        }
    }

    private void ShearInput_OnEdit(string value)
    {
        if(!string.IsNullOrEmpty(value))
        {
            ShearInputX.text = string.IsNullOrEmpty(ShearInputX.text) ? "0" : ShearInputX.text;
            ShearInputY.text = string.IsNullOrEmpty(ShearInputY.text) ? "0" : ShearInputY.text;

            Mat_A.text = "1";
            Mat_B.text = ShearInputX.text;
            Mat_C.text = ShearInputY.text;
            Mat_D.text = "1";
        }
    }

    private void DegreeInput_OnEdit(string value)
    {
        if(!string.IsNullOrEmpty(value))
        {
            float degree = float.Parse(value);

            float a = Mathf.Cos(degree * Mathf.Deg2Rad);
            float b = Mathf.Sin(degree * Mathf.Deg2Rad);
            float c = -Mathf.Sin(degree * Mathf.Deg2Rad);
            float d = Mathf.Cos(degree * Mathf.Deg2Rad);

            Mat_A.text = a.ToString();
            Mat_B.text = b.ToString();
            Mat_C.text = c.ToString();
            Mat_D.text = d.ToString();
        }
    }
}
