﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Vector2 curDist;
    private Vector2 prevDist;

    private Camera mainCamera;
    private Vector3 cameraInitialPOsition;

    public float zoomSpeed = 0.5f;

    // Start is called before the first frame update
    private void Start()
    {
        mainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        // mendeteksi ada touch
        if (Input.touchCount >= 2)
        {
            // proses ketika kedua jari bergerak
            if(Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(1).phase == TouchPhase.Moved)
            {
                //current distance between finger touches
                curDist = Input.GetTouch(0).position - Input.GetTouch(1).position;
                //difference in previous locations using delta positions
                prevDist = ((Input.GetTouch(0).position - Input.GetTouch(0).deltaPosition) - (Input.GetTouch(1).position - Input.GetTouch(1).deltaPosition));
                float touchDelta = curDist.magnitude - prevDist.magnitude;

                // ubah ukuran kamera
                mainCamera.orthographicSize -= touchDelta * zoomSpeed * Time.deltaTime;
                mainCamera.orthographicSize = Mathf.Max(mainCamera.orthographicSize, 0.1f);
            }


            // geser gambar berdasarkan perubahan touch 0
            transform.Translate(new Vector2(-Input.GetTouch(0).deltaPosition.x * Time.deltaTime, -Input.GetTouch(0).deltaPosition.y * Time.deltaTime));
        }
    }
}
